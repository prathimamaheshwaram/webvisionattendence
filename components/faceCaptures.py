import cv2
import openpyxl as xl
import pickle

cap = cv2.VideoCapture(0)
face_cascade = cv2.CascadeClassifier('./haarcascades/haarcascade_frontalface_alt2.xml')
recognizer = cv2.face.LBPHFaceRecognizer_create()
recognizer.read("trainner.yml")

labels={}
with open("labels.pickle","rb") as f:
    og_labels=pickle.load(f)
    labels={v:k for k,v in og_labels.items()}


def add_image(path,test):
    i=0

    while True:
        ret, frame = cap.read()
        grey = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(grey, scaleFactor=1.5, minNeighbors=5)
        for (x, y, w, h) in faces:
            roi_grey = grey[y:y+h, x:x+w]
            if test:
                id_,conf = recognizer.predict(roi_grey)
                if conf>=45 and conf<=80:
                    print(labels[id_],conf)
            img_item = f"{path}/{i}.png"
            i=i+1
            cv2.imwrite(img_item, roi_grey)
            color = (255, 0, 0)
            stroke = 2
            end_cord_x = x+w
            end_cord_y = y+h
            cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
        cv2.imshow('frame', frame)

        if cv2.waitKey(20) & 0xFF == ord('q') or i>100:
            break

    cap.release()
    cv2.destroyAllWindows()


def add_details_to_spreadsheet(detail):

    wb = xl.load_workbook('ITA.xlsx')
    ws = wb.active
    next_row=ws.max_row+1
    for col in range(1,3):
        cell=ws.cell(next_row,col)
        cell.value=detail[col-1]
    wb.save('ITA.xlsx')


