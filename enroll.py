#import cv2
from pathlib import Path
#import openpyxl as xl
from components import faceCaptures


rollnumber=input('roll number ')
name=input('name ')
faceCaptures.add_details_to_spreadsheet([rollnumber,name])
path=Path(f'images/{rollnumber}')

if not path.exists():
    path.mkdir()
faceCaptures.add_image(path=path,test=False)

