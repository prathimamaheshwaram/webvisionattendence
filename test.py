from components import faceCaptures
from pathlib import Path
import datetime
import cv2

cap = cv2.VideoCapture(0)
face_cascade = cv2.CascadeClassifier('./haarcascades/haarcascade_frontalface_alt2.xml')

x = datetime.datetime.now()
folderName=x.strftime("%x").replace('/','_')
print(folderName)

path=Path(f'test_images/{folderName}')

if not path.exists():
    path.mkdir()


faceCaptures.add_image(path=path,test=True)